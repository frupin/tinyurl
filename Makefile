.PHONY: redis-start redis-start-with-persistence redis-stop

REDIS_CONT_NAME ?= redis-tiny-store
redis-start:
	docker run --name $(REDIS_CONT_NAME) -d redis

redis-stop:
	docker stop $(REDIS_CONT_NAME) && docker rm $(REDIS_CONT_NAME)

PERSITENT_FOLDER ?= /tmp/redis-persistence
redis-start-with-persistence: $(PERSISTENT_FOLDER)
	docker run --name redis -d redis redis-server --appendonly yes \
		-v $(PERSISTENT_FOLDER):/data

IMAGE = tiny-url
TAG = 0.0.0

.PHONY: build
ENGINE ?= 0
build: Dockerfile
	DOCKER_BUILDKIT=$(ENGINE) docker build -t $(IMAGE):$(TAG) .

.PHONY: test-store
FOLDER = ./store
test-store: build
	docker run $(IMAGE):$(TAG)  \
		make -f Makefile.gobuild all TESTS_LOC=$(FOLDER)

.PHONY: start-stack
PERSISTENT_FOLDER ?=  /tmp/redis-persistence
start-stack: $(PERSISTENT_FOLDER)
	docker-compose up

.PHONY: stop-stack
stop-stack:
	docker-compose stop