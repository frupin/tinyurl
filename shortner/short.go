package shortner

import (
	"crypto/sha256"

	"github.com/btcsuite/btcutil/base58"
)

// CreateShortLink encodes the link to base58 and returns th 10th first characters
// to avoid too long link.
// As an improvement we should set the lenght of returned string
func CreateShortLink(link string) string {
	shortLink := base58.Encode(hashToSha256(link))

	return shortLink[:10]
}

// We use a hash function to get some entropy on on url otherwise we will get
// many collisions with base58 encoding
// We use sha256 because it is better for collision, but we could use sha1 if
// we have performance concerns. Here the hash fingerprint is not security wide.
func hashToSha256(input string) []byte {
	hash := sha256.New()
	hash.Write([]byte(input))
	return hash.Sum(nil)
}
