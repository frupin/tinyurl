package shortner

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateShortLink(t *testing.T) {
	got := CreateShortLink("www.digitalocean.com/community/tutorials/using-ldflags-to-set-version-information-for-go-applicationq")
	want := "7Mh1KV9cc6"

	assert.Equal(t, got, want)
}

func BenchmarkCreateShortLink(t *testing.B) {
	CreateShortLink("www.digitalocean.com/community/tutorials/using-ldflags-to-set-version-information-for-go-applicationq")
}
