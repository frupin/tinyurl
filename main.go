package main

import (
	"net/http"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/frupin/tinyurl/handlers"
	"gitlab.com/frupin/tinyurl/storage"
)

// Timeout is defined for the http client
const maxTimeout = 900
const defaultTimeout = 600

func main() {

	//runtime.GOMAXPROCS(1)
	app := cli.NewApp()
	app.Name = "tinyURL"
	app.Usage = "tinyURL allows you to create and use short urls."

	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:   "timeout",
			Value:  defaultTimeout,
			Usage:  "Tiny timeout: in seconds",
			EnvVar: "TINY_TIMEOUT",
		},
		cli.StringFlag{
			Name:   "tiny-port",
			Value:  "80",
			Usage:  "Tiny port number: 0-65535",
			EnvVar: "TINY_PORT",
		},
		cli.StringFlag{
			Name:   "tiny-listen",
			Value:  "0.0.0.0",
			Usage:  "IP address on which the tinyy listens: 0.0.0.0",
			EnvVar: "TINY_LISTEN",
		},
		cli.StringFlag{
			Name:   "store-port",
			Value:  "6379",
			Usage:  "Backend store port number: default to redis.",
			EnvVar: "STORE_PORT",
		},
		cli.StringFlag{
			Name:   "store-listen",
			Value:  "127.0.0.1",
			Usage:  "IP address on which the store listens: 127.0.0.1",
			EnvVar: "STORE_LISTEN",
		},
	}
	app.Action = func(c *cli.Context) error {
		log.SetFormatter(&log.JSONFormatter{})
		log.Info("starting tiny-url")

		timeout := uint64(c.Int("timeout"))
		if timeout > maxTimeout {
			timeout = maxTimeout
		}

		params := handlers.TinyParameters{
			Store:   storage.NewRedisBackend(c.String("store-listen"), c.String("store-port"), 9000),
			Timeout: time.Second * time.Duration(int64(c.Int("timeout"))),
		}
		server := handlers.NewTiny(params)
		listen := c.String("tiny-listen") + ":" + c.String("tiny-port")
		log.Info("tiny-log starts listening on : ", listen)

		return http.ListenAndServe(listen, server)
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Error(err)
		os.Exit(-1)
	}
}
