package handlers

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/frupin/tinyurl/storage"
)

func TestRedirectHandler(t *testing.T) {
	tests := []struct {
		name    string
		method  string
		uri     string
		timeout time.Duration
		want    int
	}{
		{
			name:    "Test redirect Handler",
			method:  http.MethodGet,
			uri:     "/oioeipoi",
			timeout: time.Second * time.Duration(600),
			want:    200,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			request, _ := http.NewRequest(test.method, test.uri, nil)
			params := TinyParameters{
				Store:   storage.StoreStub{},
				Timeout: time.Second * test.timeout,
			}
			response := httptest.NewRecorder()

			params.redirectHandler(response, request)
			got := response.Code
			assert.Equal(t, test.want, got)
		})
	}
}

func BenchmarkRedirectHandler(t *testing.B) {
	test := struct {
		name    string
		method  string
		uri     string
		timeout time.Duration
		want    int
	}{

		name:    "Test redirect Handler",
		method:  http.MethodGet,
		uri:     "/oioeipoi",
		timeout: time.Second * time.Duration(600),
		want:    200,
	}
	request, _ := http.NewRequest(test.method, test.uri, nil)
	params := TinyParameters{
		Store:   storage.StoreStub{},
		Timeout: time.Second * test.timeout,
	}
	response := httptest.NewRecorder()

	params.redirectHandler(response, request)
}

func TestCreateShortLink(t *testing.T) {
	tests := []struct {
		name    string
		method  string
		uri     string
		timeout time.Duration
		want    string
	}{
		{
			name:    "Test redirect Handler",
			method:  http.MethodPost,
			uri:     "/new",
			timeout: time.Second * time.Duration(600),
			want:    "HSBbM2qgRx",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			postBody, _ := json.Marshal(map[string]string{
				"originalUrl": "http://www.google.ca",
			})
			body := bytes.NewBuffer(postBody)
			request, _ := http.NewRequest(test.method, test.uri, body)
			params := TinyParameters{
				Store:   storage.StoreStub{},
				Timeout: time.Second * test.timeout,
			}
			response := httptest.NewRecorder()

			params.shortnerHandler(response, request)
			t.Log(response)
			bodyBytes, err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Fatal(err)
			}
			bodyString := string(bodyBytes)
			t.Log(response.Code)

			assert.Equal(t, test.want, bodyString)
		})
	}
}
