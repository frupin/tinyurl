package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/frupin/tinyurl/shortner"
	"gitlab.com/frupin/tinyurl/storage"
)

type TinyParameters struct {
	Timeout time.Duration
	Store   storage.StoreManager
}

func NewTiny(t TinyParameters) *mux.Router {
	forward := http.HandlerFunc(t.redirectHandler)
	shortnedURL := http.HandlerFunc(t.shortnerHandler)

	tiny := mux.NewRouter()

	tiny.Methods("POST").PathPrefix("/new").Handler(shortnedURL)
	tiny.Methods("GET").PathPrefix("/").Handler(forward)

	return tiny
}

func (p TinyParameters) redirectHandler(w http.ResponseWriter, r *http.Request) {
	// Extract the short link
	uri := r.URL.RequestURI()
	lenUri := len(uri)
	shortLink := uri[1:lenUri]
	fmt.Println(shortLink)

	// Get the original URL
	details, err := p.Store.GetURL(shortLink)
	if err != nil {
		log.Println("Error while parsing URL ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(""))
		return
	}
	log.Println("DETAILS ", details)
	var linkDetails storage.URLDetails
	err = json.Unmarshal([]byte(details), &linkDetails)
	if err != nil {
		log.Println("Error while unmarshaling URL ", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println("DETAILS ", linkDetails)

	userID := r.Header.Get("UserID")
	if userID == "" {
		// let's define a default value if the header is missing
		userID = "anonymous"
	}
	if linkDetails.Access == nil {
		linkDetails.Access = make(map[string][]int64)
	}
	if _, ok := linkDetails.Access[userID]; !ok {
		log.Println("init ", userID)
		linkDetails.Access[userID] = make([]int64, 0)
		log.Println("1 After Init ", linkDetails.Access[userID])
	}
	log.Println("After Init ", linkDetails.Access[userID])

	linkDetails.Access[userID] = append(linkDetails.Access[userID], time.Now().Unix())

	//update the store entry
	b, err := json.Marshal(linkDetails)
	if err != nil {
		log.Println("error during marshaling ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(""))
		return
	}
	log.Println(string(b))
	// It should be improved to support concurrent updates for the serialized data.
	// because as it is coded here we could lose some timestampsdue to unhandled
	// concurrent access.
	err = p.Store.PushURL(shortLink, string(b))
	if err != nil {
		log.Println("error while inserting resutls in the storage ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(""))
		return
	}
	// Rederict to the Original URL
	http.Redirect(w, r, linkDetails.OriginalURL, http.StatusSeeOther)
}

// shortnerHandler takes a JSON in a post request to create a short link and store it
// into the storage backend. The store is defined as an interface letting
// flexibility to make use of any storage we want or also to mock for unittests
// (see the handlers test.go for mock example)
func (p TinyParameters) shortnerHandler(w http.ResponseWriter, r *http.Request) {
	// By making use of a header to get the user info we delegate the authentication
	// part to another service with could be a reverse proxy such as nginx
	// set up for authenticating user with OIDC or any choice.
	// It would be the responsibility of such proxy to forge the new Headers.
	var userID string
	if userID = r.Header.Get("UserId"); userID == "" {
		// let's define a default value if the header is missing
		userID = "anonymous"
	}
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var details storage.URLDetails
	err = json.Unmarshal(b, &details)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println(details.OriginalURL)
	// URL validation
	u, err := url.ParseRequestURI(details.OriginalURL)
	if err != nil {
		log.Println("Error while parsing URL ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(""))
		return
	}

	// Create the short link
	shortLink := shortner.CreateShortLink(u.String())

	// lookup the storage. If the shortlink already exists no need to create
	// just return the link
	short, _ := p.Store.GetURL(shortLink)
	if short != "" {
		w.WriteHeader(200)
		w.Write([]byte(shortLink))
		return

	}

	// if the link does not exist create it.
	now := time.Now()
	details.Creation = storage.CreationInfos{
		UserID:    userID,
		Timestamp: now.Unix(),
	}
	b, _ = json.Marshal(details)
	if err != nil {
		log.Println("error during marshaling ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(""))
		return
	}
	details.Access = make(map[string][]int64)

	// store the details in the storage backendi
	err = p.Store.PushURL(shortLink, string(b))
	if err != nil {
		log.Println("error while inserting resutls in the storage ", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(""))
		return
	}
	short, _ = p.Store.GetURL(shortLink)
	log.Println("These infos inserted: ", short)

	// return the link to the user
	log.Println(shortLink)
	w.WriteHeader(200)
	w.Write([]byte(shortLink))
}
