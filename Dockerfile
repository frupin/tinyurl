FROM golang:1.16 as builder

WORKDIR  /go/src/gitlab.com/frupin/tiny-url
COPY *.* ./
COPY storage storage
COPY shortner shortner
COPY handlers handlers
RUN ls -lisa
RUN go get github.com/go-redis/redis/v8
RUN go get github.com/stretchr/testify/assert
RUN go get github.com/btcsuite/btcutil/base58
RUN go get gitlab.com/frupin/tinyurl
RUN go mod tidy && go mod download
RUN make -f Makefile.gobuild build-tiny
RUN cp tiny-url /usr/local/bin/tiny-url

FROM debian:buster-slim
WORKDIR /
COPY --from=builder  /usr/local/bin/tiny-url /usr/local/bin/tiny-url
RUN apt-get update && apt-get install -y curl ca-certificates

CMD ["tiny-url"]