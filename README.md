#USAGE

start:
docker-compose up

stop:
docker-compose stop


## Example with curl:
```
$ curl -XPOST -H "UserID: test" -H "Content-Type: application/json" http://127.0.0.1:80/new -d '{"originalUrl": "https://www.google.com"}'
Cc4PMy5iGt 
```
```
$ curl -v -H "UserID: test" 127.0.0.1:80/Cc4PMy5iGt*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to 127.0.0.1 (127.0.0.1) port 80 (#0)
> GET /Cc4PMy5iGt HTTP/1.1
> Host: 127.0.0.1
> User-Agent: curl/7.54.0
> Accept: */*
> UserID: test
> 
< HTTP/1.1 303 See Other
< Content-Type: text/html; charset=utf-8
< Location: https://www.google.com
< Date: Fri, 11 Jun 2021 00:49:17 GMT
< Content-Length: 49
< 
<a href="https://www.google.com">See Other</a>.

* Connection #0 to host 127.0.0.1 left intact

```

example of informations stored:
```
tiny        | 2021/06/11 00:46:02 {"originalUrl":"https://www.google.vow","creation":{"userId":"test","time":1623372324},"access":{"anonymous":[1623372335,1623372337,1623372338,1623372339],"test":[1623372361,1623372362]}}
```

## with a browser
paste this url in your browser
`127.0.0.1/Cc4PMy5iGt`
