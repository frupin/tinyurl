package storage

type StoreManager interface {
	PushURL(string, string) error
	GetURL(string) (string, error)
}

type URLPusher interface {
	PushURL(string, string) error
}

type URLGetter interface {
	GetURL(string) (string, error)
}

type StoreStub struct{}

func (s StoreStub) PushURL(host, port string) error {
	return nil
}
func (s StoreStub) GetURL(link string) (string, error) {
	return "www.google.com", nil
}

// URLDetails is a structure for holding information about an URL
type URLDetails struct {
	// OriginalURL stores the string of the requested URL that has been shortened
	OriginalURL string `json:"originalUrl"`
	// Creation gives the information about the creator of the that entry
	Creation CreationInfos `json:"creation"`
	// Access keep track of which and when users request this URL
	// the UserID will be the identifier for that map
	Access map[string][]int64 `json:"access"`
}

// CreationInfos stores informations about the URL creation
// Only used as nested struct in the URLDetails struct
type CreationInfos struct {
	// UserID identifies the user
	UserID string `json:"userId"`
	// Time registers when the URL has been created
	Timestamp int64 `json:"time"`
}

type AccessTime []int64
