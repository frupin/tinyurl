package storage

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPushURL(t *testing.T) {
	if os.Getenv("SKIP_TEST") == "true" {
		t.Skip("Skipping test")
	}
	short := "5V99ibYDSE"
	original := "https://www.google.fr"
	store := NewRedisBackend("localhost", "6379", 10)
	err := store.PushURL(short, original)
	assert.Equal(t, err, nil)
}

func BenchmarkPushURL(b *testing.B) {
	short := "cghghdghd"
	original := "www.google.com"
	store := NewRedisBackend("localhost", "6379", 10)
	store.PushURL(short, original)
}

func TestGetURL(t *testing.T) {
	if os.Getenv("SKIP_TEST") == "true" {
		t.Skip("Skipping test")
	}
	short := "5V99ibYDSE"
	original := "https://www.google.fr"
	store := NewRedisBackend("localhost", "6379", 10)
	result, _ := store.GetURL(short)
	assert.Equal(t, result, original)
}

func BenchmarkGetURL(b *testing.B) {
	short := "cghghdghd"
	store := NewRedisBackend("localhost", "6379", 10)
	store.GetURL(short)
}
