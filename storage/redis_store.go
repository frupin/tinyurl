package storage

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

type RedisBackend struct {
	client *redis.Client
	cache  time.Duration
}

//func NewRedisBackend(host, port string, duration time.Duration) StoreManager {
func NewRedisBackend(host, port string, duration time.Duration) StoreManager {
	client := redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: "",
		DB:       0,
	})

	_, err := client.Ping().Result()
	if err != nil {
		panic(fmt.Sprintf("failed to init Redis: %v", err))
	}

	return &RedisBackend{
		client: client,
		cache:  duration * time.Hour,
	}
}

func (r RedisBackend) PushURL(short, original string) error {
	err := r.client.Set(short, original, r.cache).Err()
	if err != nil {
		return fmt.Errorf("failed while saving url | Error: %q - short: %s - original: %s\n", err.Error(), short, original)
	}
	return nil
}

func (r RedisBackend) GetURL(short string) (string, error) {
	result, err := r.client.Get(short).Result()
	if err != nil {
		return "", fmt.Errorf("failed getting url | Error: %q - short: %s\n", err.Error(), short)
	}

	return result, nil
}
