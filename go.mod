module gitlab.com/frupin/tinyurl

go 1.16

require (
	github.com/btcsuite/btcutil v1.0.2
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli v1.22.5
)
